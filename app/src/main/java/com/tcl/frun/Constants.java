package com.tcl.frun;

/**
 * Created by weiminding on 3/6/17.
 */

public class Constants {
    public static final String USER_DATA_DISTANCE = "user_data_distance";
    public static final String USER_DATA_DURATION = "user_data_duration";
    public static final String USER_DATA_PACE = "user_data_pace";
    public static final String USER_DATA_CALS = "user_data_cals";
}
