package com.tcl.frun.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class PrefUtils {


    public static final String PREFERENCE_NAME = "Frun";

    public static boolean saveIntValue(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static boolean saveLongValue(Context context, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static boolean saveStringValue(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        return editor.commit();

    }

    public static int getIntValue(Context context, String key, int defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        return preferences.getInt(key, defaultValue);
    }

    public static long getLongValue(Context context, String key, int defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        return preferences.getLong(key, defaultValue);
    }

    public static String getStringValue(Context context, String key, String defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        return preferences.getString(key, defaultValue);
    }

    public static boolean saveBooleanValue(Context context, String key, boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static Boolean getBooleanValue(Context context, String key, boolean defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, 0);
        return preferences.getBoolean(key, defaultValue);
    }


    public static void addUserData(Context context, String key, long value) {
        long prevData = getLongValue(context, key,0);
        long curData = prevData + value;
        saveLongValue(context, key, curData);
    }








}
