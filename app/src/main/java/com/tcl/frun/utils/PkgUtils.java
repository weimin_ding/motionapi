package com.tcl.frun.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.tcl.frun.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by weiminding on 3/3/17.
 */

public class PkgUtils {

    public static void installApp(Context context) {

        try {
            checkIfinstallUnsecureApp(context);

            String path = Environment.getExternalStorageDirectory() + "/frun";
            File dir = new File(path);
            String tempFileName = "runningman.apk";
            String fileName = null;
            if (dir.mkdirs() || dir.isDirectory()) {
                fileName =  path + File.separator +tempFileName ;
                CopyRAWtoSDCard(context, R.raw.runningman, fileName);
            }


            Log.e("Frun", "Apk path " + fileName);


            Intent promptInstall;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", new File(fileName));
                promptInstall = new Intent(Intent.ACTION_VIEW)
                        .setDataAndType(uri,
                                "application/vnd.android.package-archive");
                promptInstall.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } else {
                Uri apkUri = Uri.parse(fileName);
                promptInstall = new Intent(Intent.ACTION_VIEW);
                promptInstall.setDataAndType(apkUri, "application/vnd.android.package-archive");
                promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }


            PackageManager packageManager = context.getPackageManager();
            if (promptInstall.resolveActivity(packageManager) != null) {
                context.startActivity(promptInstall);
            } else {
                Log.e("FRUN_POC", "No Activity available to install app");
                Toast.makeText(context, "No Activity available to install app", Toast.LENGTH_LONG).show();
            }


            //context.startActivity(Intent.createChooser(promptInstall, "sdsds"));



        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }


    private static void checkIfinstallUnsecureApp(Context context) {
        int result = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 0);
        if (result == 0) {
            Toast.makeText(context, "Need to turn on seting to install no market app", Toast.LENGTH_LONG).show();
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_SETTINGS);
            context.startActivity(intent);
        }
    }

    private static void CopyRAWtoSDCard(Context context, int input, String path) throws IOException {
        InputStream in = context.getResources().openRawResource(input);
        FileOutputStream out = new FileOutputStream(path);
        byte[] buff = new byte[1024];
        int read = 0;
        try {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } finally {
            in.close();
            out.close();
        }
    }
}
