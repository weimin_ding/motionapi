package com.tcl.frun.api;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.tcl.frun.Constants;
import com.tcl.frun.utils.PrefUtils;


public class ServiceAIDLUserMotion extends Service implements SensorEventListener{


    private final static String TAG = "Frun";
    private IFrunAidlInterface mListener;
    private MotionBinder motionBinder;
    private SensorManager sensorMan;
    private Sensor acceleroMeter, linearMeter, gravityMeter;
    private float[] mGravity;
    private double mAccel;
    private double mAccelCurrent;
    private double mAccelLast;
    private double mAccelCur;
    private double mAccelPrev;
    private int hitCount = 0;
    private double hitSum = 0;
    private double hitResult = 0;

    private final int SAMPLE_SIZE = 80; // change this sample size as you want, higher is more precise but slow measure.
    private final double THRESHOLD = 0.2; // change this threshold as you want, higher is more spike movement



    /**
     * Earth gravity is around 9.8 m/s^2 but user may not completely direct his/her hand vertical
     * during the exercise so we leave some room. Basically if the x-component of gravity, as
     * measured by the Gravity sensor, changes with a variation (delta) > GRAVITY_THRESHOLD,
     * we consider that a successful count.
     */
    private static final float GRAVITY_THRESHOLD = 7.0f;
    /** How long to keep the screen on when no activity is happening **/
    private static final long SCREEN_ON_TIMEOUT_MS = 20000; // in milliseconds

    /** an up-down movement that takes more than this will not be registered as such **/
    private static final long MAX_TIME_THRESHOLD_NS = 2000000000; // in nanoseconds (= 2sec)
    private static final long MIN_TIME_THRESHOLD_NS = 500000000;
    private static final long GAP_TIME_THRESHOLD_NS = 50000000;// in nanoseconds (= 0.05sec)
    private boolean mUp = false;
    private long mLastTime = 0;
    private long mLastTimeAcce = 0;


    public class MotionBinder extends IGameAidlInterface.Stub
    {


        @Override
        public void start(String gameId, IFrunAidlInterface listener ) throws RemoteException {
            sensorMan.registerListener(ServiceAIDLUserMotion.this, acceleroMeter,
                    SensorManager.SENSOR_DELAY_NORMAL);

            sensorMan.registerListener(ServiceAIDLUserMotion.this,  linearMeter,
                    SensorManager.SENSOR_DELAY_NORMAL);

            sensorMan.registerListener(ServiceAIDLUserMotion.this,  gravityMeter,
                    SensorManager.SENSOR_DELAY_UI);
            mListener = listener;
            mListener.onInit();
        }

        @Override
        public void stop(String gameId) throws RemoteException {
            sensorMan.unregisterListener(ServiceAIDLUserMotion.this);
        }

        @Override
        public void report(UserData userData) throws RemoteException {
            Log.w(TAG, "UserData: " + userData.toString());
            PrefUtils.addUserData(ServiceAIDLUserMotion.this, Constants.USER_DATA_DISTANCE, userData.distance);
            PrefUtils.addUserData(ServiceAIDLUserMotion.this, Constants.USER_DATA_DURATION, userData.duration);
            PrefUtils.addUserData(ServiceAIDLUserMotion.this, Constants.USER_DATA_PACE, userData.pace);
            PrefUtils.addUserData(ServiceAIDLUserMotion.this, Constants.USER_DATA_CALS, userData.cals);
        }

    }

    public ServiceAIDLUserMotion() {
    }

    @Override
    public void onCreate() {

        super.onCreate();
        motionBinder = new MotionBinder();
        sensorMan = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        acceleroMeter = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        linearMeter = sensorMan.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gravityMeter = sensorMan.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;


    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return motionBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        try {
            handleMoving(event);
            handleJump2(event);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    private void handleJump(SensorEvent event) throws RemoteException {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            //Log.e(TAG, "enter handle Jump");
            float xValue = Math.abs(event.values[0])>0.35?event.values[1]:0;
//            float yValue = Math.abs(event.values[1])>0.3?event.values[1]:0;
//            float zValue = Math.abs(event.values[2])>0.3?event.values[1]:0;
//            tvJump.setText(xValue+"\n"+yValue+"\n"+zValue);

            if ((Math.abs(xValue) > GRAVITY_THRESHOLD)) {
                long gap = event.timestamp - mLastTime;

                    if (gap < MAX_TIME_THRESHOLD_NS && mUp != (xValue > 0)) {

                        if (mUp) {
                            //tvJump.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            //Jump detected
                            mListener.onJump();
                        } else {
                            //tvJump.setBackgroundColor(getResources().getColor(R.color.colorGray));
                        }

                    }
                    mUp = xValue > 0;
                    mLastTime = event.timestamp;

            }
        }
    }

    private void handleJump2(SensorEvent event) throws RemoteException {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

            float xValue = Math.abs(event.values[0])>0.25?event.values[0]:0;
            float yValue = Math.abs(event.values[1])>0.3?event.values[1]:0;
            float zValue = Math.abs(event.values[2])>0.3?event.values[2]:0;
//            tvJump.setText(xValue+"\n"+yValue+"\n"+zValue);
            long gap = event.timestamp - mLastTime;
            if ((Math.abs(xValue) > GRAVITY_THRESHOLD) && gap > GAP_TIME_THRESHOLD_NS  ) {
                Log.e(TAG, "enter handle Jump x : " + xValue +" y : "+ yValue + " z : "+zValue);

                if ( mUp != (xValue > 0)) {

                    if (mUp) {
                        //tvJump.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        //Jump detected
                        mListener.onJump();
                    } else {
                        //tvJump.setBackgroundColor(getResources().getColor(R.color.colorGray));
                    }

                }
                mUp = xValue > 0;
                mLastTime = event.timestamp;

            }
        }
    }

    private void handleMoving(SensorEvent event) throws RemoteException {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values.clone();
            // Shake detection
            double x = mGravity[0];
            double y = mGravity[1];
            double z = mGravity[2];

//            float xValue = Math.abs(event.values[0])>0.3?event.values[1]:0;
//            float yValue = Math.abs(event.values[1])>0.3?event.values[1]:0;
//            float zValue = Math.abs(event.values[2])>0.3?event.values[1]:0;
//            tvMoving.setText(xValue+"\n"+yValue+"\n"+zValue);

            mAccelLast = mAccelCurrent;
            mAccelCurrent = Math.sqrt(x * x + y * y + z * z);
            double delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;

            if (hitCount <= SAMPLE_SIZE) {
                hitCount++;
                hitSum += Math.abs(mAccel);
            } else {
                hitResult = hitSum / SAMPLE_SIZE;

                //Log.d(TAG, String.valueOf(hitResult));

                if (hitResult > THRESHOLD) {
                    //Log.d(TAG, "Walking");
                    mListener.onRun();
                    //Toast.makeText(this,"walking", Toast.LENGTH_SHORT).show();
                } else {
                    //Log.d(TAG, "Stop Walking");
                    mListener.onStand();
                }

                hitCount = 0;
                hitSum = 0;
                hitResult = 0;
            }
        }
    }




}
